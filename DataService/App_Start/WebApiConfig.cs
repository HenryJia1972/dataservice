﻿using DataService.Models;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using System.Linq;
using System.Web.Http;

namespace DataService
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();
            config.Select().Expand().Filter().OrderBy().MaxTop(100).Count();

            ODataModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<Category>("Categories");
            builder.EntitySet<Customer>("Customers");
            builder.EntitySet<Product>("Products");
            builder.EntitySet<Employee>("Employees");
            builder.EntitySet<Order>("Orders");
            builder.EntitySet<Territory>("Territories");
            builder.EntityType<Order_Detail>().HasKey(t => new { t.OrderID, t.ProductID });
            builder.EntitySet<Order_Detail>("Order_Details");
            builder.EntitySet<Shipper>("Shippers");
            builder.EntitySet<Supplier>("Suppliers");
            builder.EntitySet<Region>("Regions");

            //builder.EntitySet<Alphabetical_list_of_product>("Alphabetical_list_of_products");
            //builder.EntitySet<Category_Sales_for_1997>("Category_Sales_for_1997");
            //builder.EntitySet<Current_Product_List>("Current_Product_List");
            //builder.EntitySet<Customer_and_Suppliers_by_City>("Customer_and_Suppliers_by_City");
            //builder.EntityType<Invoice>().HasKey(t => new { t.OrderID, t.ProductID });
            //builder.EntitySet<Invoice>("Invoices");

            config.MapODataServiceRoute(
                routeName: "ODataRoute",
                routePrefix: "odata",
                model: builder.GetEdmModel());

            // Web API configuration and services

            // Web API routes

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
        }
    }
}
