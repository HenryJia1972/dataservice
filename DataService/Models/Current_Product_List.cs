namespace DataService.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    // Notes: comes from view, modify key related items and only mark ProductID as the primary key
    [Table("Current Product List")]
    public partial class Current_Product_List
    {
        [Key]
        //[Column(Order = 0)]
        public int ProductID { get; set; }

        //[Key]
        //[Column(Order = 1)]
        [StringLength(40)]
        public string ProductName { get; set; }
    }
}
