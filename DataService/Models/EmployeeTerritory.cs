﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DataService.Models
{
    public class EmployeeTerritory
    {

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        //public EmployeeTerritory()
        //{
        //    Employees = new HashSet<Employee>();
        //}

        [Key, Column(Order = 0)]
        public int EmployeeID { get; set; }

        [Key, Column(Order = 1)]
        [StringLength(20)]
        public string TerritoryID { get; set; }

        public virtual Territory Territory { get; set; }

        public virtual Employee Employee { get; set; }
    }
}