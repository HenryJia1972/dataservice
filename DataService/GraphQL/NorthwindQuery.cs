﻿using DataService.Models;
using GraphQL;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataService.GraphQL
{
    public class CategoryType : ObjectGraphType<Category>
    {
        public CategoryType()
        {
            Field(x => x.CategoryID);
            Field(x => x.CategoryName);
            Field(x => x.Description);
            Field("Picture", x => Convert.ToBase64String(x.Picture));
            Field<ListGraphType<ProductType>>("Products");
        }
    }

    public class CustomerType : ObjectGraphType<Customer>
    {
        public CustomerType()
        {
            Field(x => x.CustomerID);
            Field(x => x.CompanyName);
            Field(x => x.ContactName);
            Field(x => x.ContactTitle);
            Field(x => x.Address);
            Field(x => x.City);
            Field(x => x.Region);
            Field(x => x.PostalCode);
            Field(x => x.Country);
            Field(x => x.Phone);
            Field(x => x.Fax);
            Field<ListGraphType<OrderType>>("Orders");
        }
    }

    public class EmployeeType : ObjectGraphType<Employee>
    {
        public EmployeeType()
        {
            Field(x => x.EmployeeID);
            Field(x => x.LastName);
            Field(x => x.FirstName);
            Field(x => x.Title);
            Field(x => x.TitleOfCourtesy);
            Field(x => x.BirthDate, true);
            Field(x => x.HireDate, true);
            Field(x => x.Address);
            Field(x => x.City);
            Field(x => x.Region);
            Field(x => x.PostalCode);
            Field(x => x.Country);
            Field(x => x.HomePhone);
            Field(x => x.Extension);
            Field(x => x.Notes);
            Field(x => x.ReportsTo, true);
            Field(x => x.PhotoPath);
            Field("Photo", x => Convert.ToBase64String(x.Photo));
            Field<EmployeeType>("Employee1");
            Field<ListGraphType<EmployeeType>>("Employees1");
            Field<ListGraphType<OrderType>>("Orders");
            Field<ListGraphType<TerritoryType>>("Territories");
        }
    }

    public class OrderDetailType : ObjectGraphType<Order_Detail>
    {
        public OrderDetailType()
        {
            Field(x => x.OrderID);
            Field(x => x.ProductID);
            Field(x => x.UnitPrice);
            Field("Quantity", x => (int)x.Quantity);
            Field(x => x.Discount);
            Field<ListGraphType<OrderType>>("Order");
            Field<ProductType>("Product");
        }

    }
    public class OrderType : ObjectGraphType<Order>
    {
        public OrderType()
        {
            Field(x => x.OrderID);
            Field(x => x.CustomerID);
            Field(x => x.EmployeeID, true);
            Field(x => x.OrderDate, true);
            Field(x => x.RequiredDate, true);
            Field(x => x.ShippedDate, true);
            Field(x => x.ShipVia, true);
            Field(x => x.Freight, true);
            Field(x => x.ShipName);
            Field(x => x.ShipAddress);
            Field(x => x.ShipCity);
            Field(x => x.ShipRegion);
            Field(x => x.ShipPostalCode);
            Field(x => x.ShipCountry);
            Field<CustomerType>("Customer");
            Field<EmployeeType>("Employee");
            Field<ListGraphType<OrderDetailType>>("Order_Details");
            Field<ShipperType>("Shipper");
        }
    }

    public class ProductType : ObjectGraphType<Product>
    {
        public ProductType()
        {
            Field(x => x.ProductName).Description("Product Name");
            Field(x => x.ProductID).Description("Product ID");
            Field(x => x.SupplierID, true);
            Field(x => x.CategoryID, true);
            Field(x => x.QuantityPerUnit);
            Field(x => x.UnitPrice, true);
            Field("UnitsInStock", x => (int)x.UnitsInStock, true);
            Field("UnitsOnOrder", x => (int)x.UnitsOnOrder, true);
            Field("ReorderLevel", x => (int)x.ReorderLevel, true);
            Field(x => x.Discontinued);
            Field<CategoryType>("Category");
            Field<SupplierType>("Supplier");
            Field<ListGraphType<OrderDetailType>>("Order_Details");
        }
    }

    public class RegionType : ObjectGraphType<Region>
    {
        public RegionType()
        {
            Field(x => x.RegionID);
            Field(x => x.RegionDescription);
            Field<ListGraphType<TerritoryType>>("Territories");
        }
    }

    public class ShipperType : ObjectGraphType<Shipper>
    {
        public ShipperType()
        {
            Field(x => x.ShipperID);
            Field(x => x.CompanyName);
            Field(x => x.Phone);
            Field<ListGraphType<OrderType>>("Orders");
        }
    }

    public class SupplierType : ObjectGraphType<Supplier>
    {
        public SupplierType()
        {
            Field(x => x.SupplierID);
            Field(x => x.CompanyName);
            Field(x => x.ContactName);
            Field(x => x.ContactTitle);
            Field(x => x.Address);
            Field(x => x.City);
            Field(x => x.Region);
            Field(x => x.PostalCode);
            Field(x => x.Country);
            Field(x => x.Phone);
            Field(x => x.Fax);
            Field(x => x.HomePage);
            Field<ListGraphType<ProductType>>("Products");
        }
    }

    public class TerritoryType : ObjectGraphType<Territory>
    {
        public TerritoryType()
        {
            Field(x => x.TerritoryID);
            Field(x => x.TerritoryDescription);
            Field(x => x.RegionID);
            Field<RegionType>("Region");
            Field<ListGraphType<EmployeeType>>("Employees");
        }
    }

    public class NorthwindQuery : ObjectGraphType
    {
        private NorthwindContext _db = new NorthwindContext();

        public NorthwindQuery()
        {
            Field<ListGraphType<CategoryType>>("categories",
                  resolve: context => this.GetCategories());

            Field<CategoryType>("category",
                  arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                  resolve: context => this.GetCategory(context.GetArgument<int>("id")));

            Field<ListGraphType<CustomerType>>("customers",
                  resolve: context => this.GetCustomers());

            Field<CustomerType>("customer",
                  arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "id" }),
                  resolve: context => this.GetCustomer(context.GetArgument<string>("id")));

            Field<ListGraphType<EmployeeType>>("employees",
                  resolve: context => this.GetEmployees());

            Field<EmployeeType>("employee",
                  arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                  resolve: context => this.GetEmployee(context.GetArgument<int>("id")));

            Field<ListGraphType<OrderDetailType>>("orderdetails",
                  resolve: context => this.GetOrderDetails());

            Field<OrderDetailType>("orderdetail",
                  arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "orderID" }, new QueryArgument<IntGraphType> { Name = "productID" }),
                  resolve: context => this.GetOrderDetail(context.GetArgument<int>("orderID"), context.GetArgument<int>("productID")));

            Field<ListGraphType<OrderType>>("orders",
                  resolve: context => this.GetCategories());

            Field<OrderType>("order",
                  arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                  resolve: context => this.GetOrder(context.GetArgument<int>("id")));

            Field<ListGraphType<ProductType>>("products",
                  resolve: context => this.GetProducts());

            Field<ProductType>("product",
                  arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                  resolve: context => this.GetProduct(context.GetArgument<int>("id")));

            Field<ListGraphType<RegionType>>("regions",
                  resolve: context => this.GetRegions());

            Field<RegionType>("region",
                  arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                  resolve: context => this.GetRegion(context.GetArgument<int>("id")));

            Field<ListGraphType<ShipperType>>("shippers",
                  resolve: context => this.GetShippers());

            Field<ShipperType>("shipper",
                  arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                  resolve: context => this.GetShipper(context.GetArgument<int>("id")));

            Field<ListGraphType<SupplierType>>("suppliers",
                  resolve: context => this.GetSuppliers());

            Field<SupplierType>("supplier",
                  arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                  resolve: context => this.GetSupplier(context.GetArgument<int>("id")));

            Field<ListGraphType<TerritoryType>>("territories",
                  resolve: context => this.GetTerritories());

            Field<TerritoryType>("territory",
                  arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "id" }),
                  resolve: context => this.GetTerritory(context.GetArgument<string>("id")));

        }

        [GraphQLMetadata("categories")]
        public IEnumerable<Category> GetCategories()
        {
            return this._db.Categories.ToList();
        }

        [GraphQLMetadata("category")]
        public Category GetCategory(int id)
        {
            return this._db.Categories.FirstOrDefault(c => c.CategoryID == id);
        }

        [GraphQLMetadata("customers")]
        public IEnumerable<Customer> GetCustomers()
        {
            return this._db.Customers.ToList();
        }

        [GraphQLMetadata("customer")]
        public Customer GetCustomer(string id)
        {
            return this._db.Customers.FirstOrDefault(c => c.CustomerID == id);
        }

        [GraphQLMetadata("employees")]
        public IEnumerable<Employee> GetEmployees()
        {
            return this._db.Employees.ToList();
        }

        [GraphQLMetadata("employee")]
        public Employee GetEmployee(int id)
        {
            return this._db.Employees.FirstOrDefault(e => e.EmployeeID == id);
        }

        [GraphQLMetadata("orderdetails")]
        public IEnumerable<Order_Detail> GetOrderDetails()
        {
            return this._db.Order_Details.ToList();
        }

        [GraphQLMetadata("orderdetail")]
        public Order_Detail GetOrderDetail(int orderID, int productID)
        {
            return this._db.Order_Details.FirstOrDefault(o => o.OrderID == orderID && o.ProductID == productID);
        }

        [GraphQLMetadata("orders")]
        public IEnumerable<Order> GetOrders()
        {
            return this._db.Orders.ToList();
        }

        [GraphQLMetadata("order")]
        public Order GetOrder(int id)
        {
            return this._db.Orders.FirstOrDefault(o => o.OrderID == id);
        }

        [GraphQLMetadata("products")]
        public IEnumerable<Product> GetProducts()
        {
            return this._db.Products.ToList();
        }

        [GraphQLMetadata("product")]
        public Product GetProduct(int id)
        {
            return this._db.Products.FirstOrDefault(p => p.ProductID == id);
        }

        [GraphQLMetadata("regions")]
        public IEnumerable<Region> GetRegions()
        {
            return this._db.Regions.ToList();
        }

        [GraphQLMetadata("region")]
        public Region GetRegion(int id)
        {
            return this._db.Regions.FirstOrDefault(r => r.RegionID == id);
        }

        [GraphQLMetadata("shippers")]
        public IEnumerable<Shipper> GetShippers()
        {
            return this._db.Shippers.ToList();
        }

        [GraphQLMetadata("shipper")]
        public Shipper GetShipper(int id)
        {
            return this._db.Shippers.FirstOrDefault(s => s.ShipperID == id);
        }

        [GraphQLMetadata("suppliers")]
        public IEnumerable<Supplier> GetSuppliers()
        {
            return this._db.Suppliers.ToList();
        }

        [GraphQLMetadata("supplier")]
        public Supplier GetSupplier(int id)
        {
            return this._db.Suppliers.FirstOrDefault(s => s.SupplierID == id);
        }

        [GraphQLMetadata("territories")]
        public IEnumerable<Territory> GetTerritories()
        {
            return this._db.Territories.ToList();
        }

        [GraphQLMetadata("territory")]
        public Territory GetTerritory(string id)
        {
            return this._db.Territories.FirstOrDefault(t => t.TerritoryID == id);
        }
    }
}