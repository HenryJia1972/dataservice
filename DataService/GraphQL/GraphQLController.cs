﻿using GraphQL;
using GraphQL.Types;
using System.Threading.Tasks;
using System.Web.Http;

namespace DataService.GraphQL
{
    // TODO: Check route related items since without RouteAttribute or use [Route("graphql")] not works even renamed GraphQLController
    [Route("graphqlSvc")]
    public class GraphQLController : ApiController
    {
        public GraphQLController()
        { }

        public async Task<IHttpActionResult> Post([FromBody] GraphQLQuery query)
        {

            var schema = new Schema
            {
                Query = new NorthwindQuery()
            };
            var inputs = query.Variables.ToInputs();
            var result = await new DocumentExecuter().ExecuteAsync(_ =>
            {
                _.Inputs = inputs;
                _.Schema = schema;
                _.OperationName = query.OperationName;
                _.Query = query.Query;

            });

            if (result.Errors?.Count > 0)
            {
                return BadRequest();
            }

            return Ok(result);
        }
    }
}