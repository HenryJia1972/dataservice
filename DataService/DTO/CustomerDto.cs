﻿namespace DataService.DTO
{
    public class CustomerDto
    {
        public string CustomerID { get; set; }

        public string CompanyName { get; set; }

        public string ContactName { get; set; }

        public string ContactTitle { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string Region { get; set; }

        public string PostalCode { get; set; }

        public string Country { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }

        //public static CustomerDto ConvertFromModel(Customer source)
        //{
        //    return new CustomerDto()
        //    {
        //        CustomerID = source.CustomerID,
        //        CompanyName = source.CompanyName,
        //        ContactName = source.ContactName,
        //        ContactTitle = source.ContactTitle,
        //        Address = source.Address,
        //        City = source.City,
        //        Region = source.Region,
        //        PostalCode = source.PostalCode,
        //        Country = source.Country,
        //        Phone = source.Phone,
        //        Fax = source.Fax
        //    };
        //}
    }
}