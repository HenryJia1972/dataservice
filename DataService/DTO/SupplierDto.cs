﻿namespace DataService.DTO
{
    public class SupplierDto
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int SupplierID { get; set; }
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
        public string ContactTitle { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string HomePage { get; set; }
    }

    //public class SupplierDtoMapper : MapperBase<Supplier, SupplierDto>
    //{
    //    ////BCC/ BEGIN CUSTOM CODE SECTION 
    //    ////ECC/ END CUSTOM CODE SECTION 
    //    public override Expression<Func<Supplier, SupplierDto>> SelectorExpression
    //    {
    //        get
    //        {
    //            return ((Expression<Func<Supplier, SupplierDto>>)(p => new SupplierDto()
    //            {
    //                ////BCC/ BEGIN CUSTOM CODE SECTION 
    //                ////ECC/ END CUSTOM CODE SECTION 
    //                SupplierID = p.SupplierID,
    //                CompanyName = p.CompanyName,
    //                ContactName = p.ContactName,
    //                ContactTitle = p.ContactTitle,
    //                Address = p.Address,
    //                City = p.City,
    //                Region = p.Region,
    //                PostalCode = p.PostalCode,
    //                Country = p.Country,
    //                Phone = p.Phone,
    //                Fax = p.Fax,
    //                HomePage = p.HomePage
    //            }));
    //        }
    //    }

    //    public override void MapToModel(SupplierDto dto, Supplier model)
    //    {
    //        ////BCC/ BEGIN CUSTOM CODE SECTION 
    //        ////ECC/ END CUSTOM CODE SECTION 
    //        model.SupplierID = dto.SupplierID;
    //        model.CompanyName = dto.CompanyName;
    //        model.ContactName = dto.ContactName;
    //        model.ContactTitle = dto.ContactTitle;
    //        model.Address = dto.Address;
    //        model.City = dto.City;
    //        model.Region = dto.Region;
    //        model.PostalCode = dto.PostalCode;
    //        model.Country = dto.Country;
    //        model.Phone = dto.Phone;
    //        model.Fax = dto.Fax;
    //        model.HomePage = dto.HomePage;

    //    }
    //}
}
