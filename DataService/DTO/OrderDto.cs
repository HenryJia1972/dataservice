﻿using System;

namespace DataService.DTO
{
    public class OrderDto
    {
        public int OrderID { get; set; }
        public string CustomerID { get; set; }
        public int? EmployeeID { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? RequiredDate { get; set; }
        public DateTime? ShippedDate { get; set; }
        public int? ShipVia { get; set; }
        public decimal? Freight { get; set; }
        public string ShipName { get; set; }
        public string ShipAddress { get; set; }
        public string ShipCity { get; set; }
        public string ShipRegion { get; set; }
        public string ShipPostalCode { get; set; }
        public string ShipCountry { get; set; }

        //public static OrderDto ConvertFromModel(Order source)
        //{
        //    return new OrderDto()
        //    {
        //        OrderID = source.OrderID,
        //        CustomerID = source.CustomerID,
        //        EmployeeID = source.EmployeeID,
        //        OrderDate = source.OrderDate,
        //        RequiredDate = source.RequiredDate,
        //        ShippedDate = source.ShippedDate,
        //        ShipVia = source.ShipVia,
        //        Freight = source.Freight,
        //        ShipName = source.ShipName,
        //        ShipAddress = source.ShipAddress,
        //        ShipCity = source.ShipCity,
        //        ShipRegion = source.ShipRegion,
        //        ShipPostalCode = source.ShipPostalCode,
        //        ShipCountry = source.ShipCountry
        //    };
        //}
    }
}