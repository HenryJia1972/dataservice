﻿using System.Collections.Generic;

namespace DataService.DTO
{
    public class CategoryDetailDto
    {
        public int CategoryID { get; set; }

        public string CategoryName { get; set; }

        public string Description { get; set; }

        public byte[] Picture { get; set; }

        public IEnumerable<string> ProductNames { get; set; }

        //public static CategoryDetailDto ConvertFromModel(Category source)
        //{
        //    return new CategoryDetailDto()
        //    {
        //        CategoryID = source.CategoryID,
        //        CategoryName = source.CategoryName,
        //        Description = source.Description,
        //        Picture = source.Picture,
        //        ProductNames = source.Products.Select(p => p.ProductName)
        //    };
        //}
    }
}