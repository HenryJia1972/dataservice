﻿namespace DataService.DTO
{
    public class CategoryDto
    {
        public int CategoryID { get; set; }

        public string CategoryName { get; set; }

        public string Description { get; set; }

        //public static CategoryDto ConvertFromModel(Category source) {
        //    return new CategoryDto()
        //    {
        //        CategoryID = source.CategoryID,
        //        CategoryName = source.CategoryName,
        //        Description = source.Description
        //    };
        //}

        // public static Func<Category, CategoryDto> ConvertFromModel = DtoConverter.AsCategoryDto.Compile();
    }
}