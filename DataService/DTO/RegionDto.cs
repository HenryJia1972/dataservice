﻿namespace DataService.DTO
{
    public class RegionDto
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int RegionID { get; set; }
        public string RegionDescription { get; set; }
    }

    //public class RegionDtoMapper : MapperBase<Region, RegionDto>
    //{
    //    ////BCC/ BEGIN CUSTOM CODE SECTION 
    //    ////ECC/ END CUSTOM CODE SECTION 
    //    public override Expression<Func<Region, RegionDto>> SelectorExpression
    //    {
    //        get
    //        {
    //            return ((Expression<Func<Region, RegionDto>>)(p => new RegionDto()
    //            {
    //                ////BCC/ BEGIN CUSTOM CODE SECTION 
    //                ////ECC/ END CUSTOM CODE SECTION 
    //                RegionID = p.RegionID,
    //                RegionDescription = p.RegionDescription
    //            }));
    //        }
    //    }

    //    public override void MapToModel(RegionDto dto, Region model)
    //    {
    //        ////BCC/ BEGIN CUSTOM CODE SECTION 
    //        ////ECC/ END CUSTOM CODE SECTION 
    //        model.RegionID = dto.RegionID;
    //        model.RegionDescription = dto.RegionDescription;

    //    }
    //}
}
