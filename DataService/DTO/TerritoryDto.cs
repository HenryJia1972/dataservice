﻿namespace DataService.DTO
{
    public class TerritoryDto
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public string TerritoryID { get; set; }
        public string TerritoryDescription { get; set; }
        public int RegionID { get; set; }
    }

    //public class TerritoryDtoMapper : MapperBase<Territory, TerritoryDto>
    //{
    //    ////BCC/ BEGIN CUSTOM CODE SECTION 
    //    ////ECC/ END CUSTOM CODE SECTION 
    //    public override Expression<Func<Territory, TerritoryDto>> SelectorExpression
    //    {
    //        get
    //        {
    //            return ((Expression<Func<Territory, TerritoryDto>>)(p => new TerritoryDto()
    //            {
    //                ////BCC/ BEGIN CUSTOM CODE SECTION 
    //                ////ECC/ END CUSTOM CODE SECTION 
    //                TerritoryID = p.TerritoryID,
    //                TerritoryDescription = p.TerritoryDescription,
    //                RegionID = p.RegionID
    //            }));
    //        }
    //    }

    //    public override void MapToModel(TerritoryDto dto, Territory model)
    //    {
    //        ////BCC/ BEGIN CUSTOM CODE SECTION 
    //        ////ECC/ END CUSTOM CODE SECTION 
    //        model.TerritoryID = dto.TerritoryID;
    //        model.TerritoryDescription = dto.TerritoryDescription;
    //        model.RegionID = dto.RegionID;

    //    }
    //}
}
