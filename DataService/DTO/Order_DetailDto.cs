﻿namespace DataService.DTO
{
    public class Order_DetailDto
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int OrderID { get; set; }
        public int ProductID { get; set; }
        public decimal UnitPrice { get; set; }
        public short Quantity { get; set; }
        public float Discount { get; set; }
    }

    //public class Order_DetailDtoMapper : MapperBase<Order_Detail, Order_DetailDto>
    //{
    //    ////BCC/ BEGIN CUSTOM CODE SECTION 
    //    ////ECC/ END CUSTOM CODE SECTION 
    //    public override Expression<Func<Order_Detail, Order_DetailDto>> SelectorExpression
    //    {
    //        get
    //        {
    //            return ((Expression<Func<Order_Detail, Order_DetailDto>>)(p => new Order_DetailDto()
    //            {
    //                ////BCC/ BEGIN CUSTOM CODE SECTION 
    //                ////ECC/ END CUSTOM CODE SECTION 
    //                OrderID = p.OrderID,
    //                ProductID = p.ProductID,
    //                UnitPrice = p.UnitPrice,
    //                Quantity = p.Quantity,
    //                Discount = p.Discount
    //            }));
    //        }
    //    }

    //    public override void MapToModel(Order_DetailDto dto, Order_Detail model)
    //    {
    //        ////BCC/ BEGIN CUSTOM CODE SECTION 
    //        ////ECC/ END CUSTOM CODE SECTION 
    //        model.OrderID = dto.OrderID;
    //        model.ProductID = dto.ProductID;
    //        model.UnitPrice = dto.UnitPrice;
    //        model.Quantity = dto.Quantity;
    //        model.Discount = dto.Discount;

    //    }
    //}
}
