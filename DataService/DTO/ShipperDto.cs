﻿namespace DataService.DTO
{
    public class ShipperDto
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int ShipperID { get; set; }
        public string CompanyName { get; set; }
        public string Phone { get; set; }
    }

    //public class ShipperDtoMapper : MapperBase<Shipper, ShipperDto>
    //{
    //    ////BCC/ BEGIN CUSTOM CODE SECTION 
    //    ////ECC/ END CUSTOM CODE SECTION 
    //    public override Expression<Func<Shipper, ShipperDto>> SelectorExpression
    //    {
    //        get
    //        {
    //            return ((Expression<Func<Shipper, ShipperDto>>)(p => new ShipperDto()
    //            {
    //                ////BCC/ BEGIN CUSTOM CODE SECTION 
    //                ////ECC/ END CUSTOM CODE SECTION 
    //                ShipperID = p.ShipperID,
    //                CompanyName = p.CompanyName,
    //                Phone = p.Phone
    //            }));
    //        }
    //    }

    //    public override void MapToModel(ShipperDto dto, Shipper model)
    //    {
    //        ////BCC/ BEGIN CUSTOM CODE SECTION 
    //        ////ECC/ END CUSTOM CODE SECTION 
    //        model.ShipperID = dto.ShipperID;
    //        model.CompanyName = dto.CompanyName;
    //        model.Phone = dto.Phone;

    //    }
    //}
}
