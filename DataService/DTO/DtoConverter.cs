﻿using DataService.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace DataService.DTO
{
    public static class DtoConverter
    {
        // Typed lambda expression for Select() method.
        public static readonly Expression<Func<Category, CategoryDto>> AsCategoryDto =
            source => new CategoryDto()
            {
                CategoryID = source.CategoryID,
                CategoryName = source.CategoryName,
                Description = source.Description
            };

        public static readonly Func<Category, CategoryDto> ConvertToCategoryDto = AsCategoryDto.Compile();

        public static readonly Expression<Func<Category, CategoryDetailDto>> AsCategoryDetailDto =
            source => new CategoryDetailDto()
            {
                CategoryID = source.CategoryID,
                CategoryName = source.CategoryName,
                Description = source.Description,
                Picture = source.Picture,
                ProductNames = source.Products.Select(p => p.ProductName)
            };

        public static readonly Func<Category, CategoryDetailDto> ConvertToCategoryDetailDto = AsCategoryDetailDto.Compile();


        public static readonly Expression<Func<Product, ProductDto>> AsProductDto =
            source => new ProductDto()
            {
                ProductID = source.ProductID,
                ProductName = source.ProductName,
                SupplierID = source.SupplierID,
                CategoryID = source.CategoryID,
                QuantityPerUnit = source.QuantityPerUnit,
                UnitPrice = source.UnitPrice,
                UnitsInStock = source.UnitsInStock,
                UnitsOnOrder = source.UnitsOnOrder,
                ReorderLevel = source.ReorderLevel,
                Discontinued = source.Discontinued
            };

        public static readonly Func<Product, ProductDto> ConvertToProductDto = AsProductDto.Compile();

        public static readonly Expression<Func<Customer, CustomerDto>> AsCustomerDto =
            source => new CustomerDto()
            {
                CustomerID = source.CustomerID,
                CompanyName = source.CompanyName,
                ContactName = source.ContactName,
                ContactTitle = source.ContactTitle,
                Address = source.Address,
                City = source.City,
                Region = source.Region,
                PostalCode = source.PostalCode,
                Country = source.Country,
                Phone = source.Phone,
                Fax = source.Fax
            };

        public static readonly Func<Customer, CustomerDto> ConvertToCustomerDto = AsCustomerDto.Compile();

        public static readonly Expression<Func<Order, OrderDto>> AsOrderDto =
            source => new OrderDto()
            {
                OrderID = source.OrderID,
                CustomerID = source.CustomerID,
                EmployeeID = source.EmployeeID,
                OrderDate = source.OrderDate,
                RequiredDate = source.RequiredDate,
                ShippedDate = source.ShippedDate,
                ShipVia = source.ShipVia,
                Freight = source.Freight,
                ShipName = source.ShipName,
                ShipAddress = source.ShipAddress,
                ShipCity = source.ShipCity,
                ShipRegion = source.ShipRegion,
                ShipPostalCode = source.ShipPostalCode,
                ShipCountry = source.ShipCountry
            };

        public static readonly Func<Order, OrderDto> ConvertToOrderDto = AsOrderDto.Compile();

        public static readonly Expression<Func<Employee, EmployeeDto>> AsEmployeeDto =
            source => new EmployeeDto()
            {
                EmployeeID = source.EmployeeID,
                LastName = source.LastName,
                FirstName = source.FirstName,
                Title = source.Title,
                TitleOfCourtesy = source.TitleOfCourtesy,
                BirthDate = source.BirthDate,
                HireDate = source.HireDate,
                Address = source.Address,
                City = source.City,
                Region = source.Region,
                PostalCode = source.PostalCode,
                Country = source.Country,
                HomePhone = source.HomePhone,
                Extension = source.Extension,
                Notes = source.Notes,
                ReportsTo = source.ReportsTo,
                PhotoPath = source.PhotoPath
            };

        public static readonly Expression<Func<Order_Detail, Order_DetailDto>> AsOrderDetailDto =
            source => new Order_DetailDto()
            {
                OrderID = source.OrderID,
                ProductID = source.ProductID,
                UnitPrice = source.UnitPrice,
                Quantity = source.Quantity,
                Discount = source.Discount
            };

        public static readonly Func<Order_Detail, Order_DetailDto> ConvertToOrderDetailDto = AsOrderDetailDto.Compile();

        public static readonly Expression<Func<Region, RegionDto>> AsRegionDto =
            source => new RegionDto()
            {
                RegionID = source.RegionID,
                RegionDescription = source.RegionDescription
            };

        public static readonly Func<Region, RegionDto> ConvertToRegionDto = AsRegionDto.Compile();

        public static readonly Expression<Func<Shipper, ShipperDto>> AsShipperDto =
            source => new ShipperDto()
            {
                ShipperID = source.ShipperID,
                CompanyName = source.CompanyName,
                Phone = source.Phone
            };

        public static readonly Func<Shipper, ShipperDto> ConvertToShipperDto = AsShipperDto.Compile();

        public static readonly Expression<Func<Supplier, SupplierDto>> AsSupplierDto =
            source => new SupplierDto()
            {
                SupplierID = source.SupplierID,
                CompanyName = source.CompanyName,
                ContactName = source.ContactName,
                ContactTitle = source.ContactTitle,
                Address = source.Address,
                City = source.City,
                Region = source.Region,
                PostalCode = source.PostalCode,
                Country = source.Country,
                Phone = source.Phone,
                Fax = source.Fax,
                HomePage = source.HomePage
            };

        public static readonly Func<Supplier, SupplierDto> ConvertToSupplierDto = AsSupplierDto.Compile();

        public static readonly Func<Employee, EmployeeDto> ConvertToEmployeeDto = AsEmployeeDto.Compile();

        public static readonly Expression<Func<Territory, TerritoryDto>> AsTerritoryDto =
            source => new TerritoryDto()
            {
                TerritoryID = source.TerritoryID,
                TerritoryDescription = source.TerritoryDescription,
                RegionID = source.RegionID
            };

        public static readonly Func<Territory, TerritoryDto> ConvertToTerritoryDto = AsTerritoryDto.Compile();
    }

}