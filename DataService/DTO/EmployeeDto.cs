﻿using System;

namespace DataService.DTO
{
    public class EmployeeDto
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int EmployeeID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Title { get; set; }
        public string TitleOfCourtesy { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime? HireDate { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string HomePhone { get; set; }
        public string Extension { get; set; }
        public string Notes { get; set; }
        public int? ReportsTo { get; set; }
        public string PhotoPath { get; set; }
    }

    //public class EmployeeDtoMapper : MapperBase<Employee, EmployeeDto>
    //{
    //    ////BCC/ BEGIN CUSTOM CODE SECTION 
    //    ////ECC/ END CUSTOM CODE SECTION 
    //    public override Expression<Func<Employee, EmployeeDto>> SelectorExpression
    //    {
    //        get
    //        {
    //            return ((Expression<Func<Employee, EmployeeDto>>)(p => new EmployeeDto()
    //            {
    //                ////BCC/ BEGIN CUSTOM CODE SECTION 
    //                ////ECC/ END CUSTOM CODE SECTION 
    //                EmployeeID = p.EmployeeID,
    //                LastName = p.LastName,
    //                FirstName = p.FirstName,
    //                Title = p.Title,
    //                TitleOfCourtesy = p.TitleOfCourtesy,
    //                BirthDate = p.BirthDate,
    //                HireDate = p.HireDate,
    //                Address = p.Address,
    //                City = p.City,
    //                Region = p.Region,
    //                PostalCode = p.PostalCode,
    //                Country = p.Country,
    //                HomePhone = p.HomePhone,
    //                Extension = p.Extension,
    //                Notes = p.Notes,
    //                ReportsTo = p.ReportsTo,
    //                PhotoPath = p.PhotoPath
    //            }));
    //        }
    //    }

    //    public override void MapToModel(EmployeeDto dto, Employee model)
    //    {
    //        ////BCC/ BEGIN CUSTOM CODE SECTION 
    //        ////ECC/ END CUSTOM CODE SECTION 
    //        model.EmployeeID = dto.EmployeeID;
    //        model.LastName = dto.LastName;
    //        model.FirstName = dto.FirstName;
    //        model.Title = dto.Title;
    //        model.TitleOfCourtesy = dto.TitleOfCourtesy;
    //        model.BirthDate = dto.BirthDate;
    //        model.HireDate = dto.HireDate;
    //        model.Address = dto.Address;
    //        model.City = dto.City;
    //        model.Region = dto.Region;
    //        model.PostalCode = dto.PostalCode;
    //        model.Country = dto.Country;
    //        model.HomePhone = dto.HomePhone;
    //        model.Extension = dto.Extension;
    //        model.Notes = dto.Notes;
    //        model.ReportsTo = dto.ReportsTo;
    //        model.PhotoPath = dto.PhotoPath;

    //    }
    //}
}
