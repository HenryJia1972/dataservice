﻿using DataService.DTO;
using DataService.Models;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace DataService.ApiControllers
{

    [RoutePrefix("api/Territories")]
    public class TerritoriesApiController : ApiController
    {
        private NorthwindContext db = new NorthwindContext();

        // GET: api/Territories
        [Route("")]
        public IQueryable<TerritoryDto> GetTerritories()
        {
            return db.Territories.Select(DtoConverter.AsTerritoryDto);
        }

        // GET: api/Territories/5
        [Route("{id}")]
        [ResponseType(typeof(TerritoryDto))]
        public async Task<IHttpActionResult> GetTerritory(string id)
        {
            Territory territory = await db.Territories.FindAsync(id);
            if (territory == null)
            {
                return NotFound();
            }

            return Ok(DtoConverter.ConvertToTerritoryDto(territory));
        }

        // GET: api/Territories/5/Employees
        [Route("{id}/Employees")]
        public IQueryable<EmployeeDto> GetEmployees(string id)
        {
            // return db.Territories.Where(m => m.TerritoryID == key).SelectMany(m => m.Employees);

            return db.EmployeeTerritories
                .Where(x => x.TerritoryID == id)
                .Join(db.Employees, et => et.EmployeeID, e => e.EmployeeID, (et, e) => new { Employee = e, EmployeeTerritory = et })
                .Select(x => x.Employee)
                .Select(DtoConverter.AsEmployeeDto);
        }

        // GET: api/Territories/5/Region
        [Route("{id}/Region")]
        [ResponseType(typeof(RegionDto))]
        public async Task<IHttpActionResult> GetRegion(string id)
        {
            Territory territory = await db.Territories.FindAsync(id);
            if (territory == null)
            {
                return NotFound();
            }
            return Ok(DtoConverter.ConvertToRegionDto(territory.Region));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TerritoryExists(string id)
        {
            return db.Territories.Count(e => e.TerritoryID == id) > 0;
        }
    }
}