﻿using DataService.DTO;
using DataService.Models;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace DataService.ApiControllers
{
    [RoutePrefix("api/Employees")]
    public class EmployeesApiController : ApiController
    {
        private NorthwindContext db = new NorthwindContext();

        // GET: api/Employees
        [Route("")]
        public IQueryable<EmployeeDto> GetEmployees()
        {
            return db.Employees.Select(DtoConverter.AsEmployeeDto);
        }

        // GET: api/Employees/5
        [Route("{id:int}")]
        [ResponseType(typeof(EmployeeDto))]
        public async Task<IHttpActionResult> GetEmployee(int id)
        {
            Employee employee = await db.Employees.FindAsync(id);
            if (employee == null)
            {
                return NotFound();
            }

            return Ok(DtoConverter.ConvertToEmployeeDto(employee));
        }

        // GET: api/Employees/5/Employee1
        [Route("{id:int}/Employee1")]
        [ResponseType(typeof(EmployeeDto))]
        public async Task<IHttpActionResult> GetEmployee1(int id)
        {
            var employee = await db.Employees.SingleOrDefaultAsync(m => m.EmployeeID == id);

            if (employee == null)
            {
                return NotFound();
            }

            return Ok(DtoConverter.ConvertToEmployeeDto(employee.Employee1));
        }

        // GET: odata/Employees(5)/Employees1
        [Route("{id:int}/Employees1")]
        public IQueryable<EmployeeDto> GetEmployees1(int id)
        {
            return db.Employees.Where(m => m.ReportsTo == id).Select(DtoConverter.AsEmployeeDto);
        }

        // GET: api/Employees/5/Orders
        [Route("{id:int}/Orders")]
        public IQueryable<OrderDto> GetOrders(int id)
        {
            return db.Employees.Where(m => m.EmployeeID == id).SelectMany(m => m.Orders).Select(DtoConverter.AsOrderDto);
        }

        // GET: api/Employees/5/Territories
        [Route("{id:int}/Territories")]
        public IQueryable<TerritoryDto> GetTerritories(int id)
        {
            return db.EmployeeTerritories.Where(m => m.EmployeeID == id).Select(m => m.Territory).Select(DtoConverter.AsTerritoryDto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EmployeeExists(int id)
        {
            return db.Employees.Count(e => e.EmployeeID == id) > 0;
        }
    }
}