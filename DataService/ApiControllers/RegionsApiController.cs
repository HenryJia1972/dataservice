﻿using DataService.DTO;
using DataService.Models;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace DataService.ApiControllers
{
    [RoutePrefix("api/Regions")]
    public class RegionsApiController : ApiController
    {
        private NorthwindContext db = new NorthwindContext();

        // GET: api/Regions
        [Route("")]
        public IQueryable<RegionDto> GetRegions()
        {
            return db.Regions.Select(DtoConverter.AsRegionDto);
        }

        // GET: api/Regions/5
        [Route("{id:int}")]
        [ResponseType(typeof(RegionDto))]
        public async Task<IHttpActionResult> GetRegion(int id)
        {
            Region region = await db.Regions.FindAsync(id);
            if (region == null)
            {
                return NotFound();
            }

            return Ok(DtoConverter.ConvertToRegionDto(region));
        }

        // GET: api/Regions/5/Territories
        [Route("{id:int}/Territories")]
        public IQueryable<TerritoryDto> GetTerritories(int id)
        {
            return db.Regions.Where(m => m.RegionID == id).SelectMany(m => m.Territories).Select(DtoConverter.AsTerritoryDto);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RegionExists(int id)
        {
            return db.Regions.Count(e => e.RegionID == id) > 0;
        }
    }
}