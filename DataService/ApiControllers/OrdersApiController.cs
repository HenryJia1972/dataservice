﻿using DataService.DTO;
using DataService.Models;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace DataService.ApiControllers
{
    [RoutePrefix("api/Orders")]
    public class OrdersApiController : ApiController
    {
        private NorthwindContext db = new NorthwindContext();

        // GET: api/Orders
        [Route("")]
        public IQueryable<OrderDto> GetOrders()
        {
            return db.Orders.Select(DtoConverter.AsOrderDto);
        }

        // GET: api/Orders/5
        [Route("{id:int}")]
        [ResponseType(typeof(OrderDto))]
        public async Task<IHttpActionResult> GetOrder(int id)
        {
            Order order = await db.Orders.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }

            return Ok(DtoConverter.ConvertToOrderDto(order));
        }

        // GET: api/Orders/5/Customer
        [Route("{id:int}/Customer")]
        [ResponseType(typeof(CustomerDto))]
        public async Task<IHttpActionResult> GetCustomer(int id)
        {
            Order order = await db.Orders.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }

            return Ok(DtoConverter.ConvertToCustomerDto(order.Customer));
        }

        // GET: api/Orders/5/Employee
        [Route("{id:int}/Employee")]
        [ResponseType(typeof(EmployeeDto))]
        public async Task<IHttpActionResult> GetEmployee(int id)
        {
            Order order = await db.Orders.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }

            return Ok(DtoConverter.ConvertToEmployeeDto(order.Employee));
        }

        // GET: api/Orders/5/Order_Details
        [Route("{id:int}/Order_Details")]
        public IQueryable<Order_DetailDto> GetOrder_Details(int id)
        {
            return db.Orders.Where(m => m.OrderID == id).SelectMany(m => m.Order_Details).Select(DtoConverter.AsOrderDetailDto);
        }

        // GET: api/Orders/5/Shipper
        [Route("{id:int}/Shipper")]
        [ResponseType(typeof(ShipperDto))]
        public async Task<IHttpActionResult> GetShipper(int id)
        {
            Order order = await db.Orders.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }

            return Ok(DtoConverter.ConvertToShipperDto(order.Shipper));
        }

        // GET: api/Orders/5/Products
        [Route("{id:int}/Products")]
        public IQueryable<ProductDto> GetProducts(int id)
        {
            return db.Order_Details.Where(m => m.OrderID == id).Select(m => m.Product).Select(DtoConverter.AsProductDto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrderExists(int id)
        {
            return db.Orders.Count(e => e.OrderID == id) > 0;
        }
    }
}