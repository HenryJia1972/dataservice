﻿using DataService.DTO;
using DataService.Models;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace DataService.APIControllers
{
    [RoutePrefix("api/Customers")]
    public class CustomersApiController : ApiController
    {
        private NorthwindContext db = new NorthwindContext();

        // GET: api/Customers
        [Route("")]
        public IQueryable<CustomerDto> GetCustomers()
        {
            return db.Customers.Select(DtoConverter.AsCustomerDto);
        }

        // GET: api/Customers/5
        [Route("{id:length(5)}")]
        [ResponseType(typeof(CustomerDto))]
        public async Task<IHttpActionResult> GetCustomer(string id)
        {
            Customer customer = await db.Customers.FindAsync(id);
            if (customer == null)
            {
                return NotFound();
            }

            return Ok(DtoConverter.ConvertToCustomerDto(customer));
        }

        [Route("{id:length(5)}/orders")]
        public IQueryable<OrderDto> GetOrdersOfCustomer(string id)
        {
            var orders = db.Customers.Where(m => m.CustomerID == id).SelectMany(m => m.Orders);

            return orders.Select(DtoConverter.AsOrderDto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CustomerExists(string id)
        {
            return db.Customers.Count(e => e.CustomerID == id) > 0;
        }
    }
}