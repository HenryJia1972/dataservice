﻿using DataService.DTO;
using DataService.Models;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace DataService.APIControllers
{
    [RoutePrefix("api/Categories")]
    public class CategoriesApiController : ApiController
    {
        private NorthwindContext db = new NorthwindContext();

        // GET: api/Categories
        [Route("")]
        public IQueryable<CategoryDto> GetCategories()
        {
            return db.Categories.Select(DtoConverter.AsCategoryDto);
        }

        // GET: api/Categories/5
        [Route("{id:int}")]
        [ResponseType(typeof(CategoryDto))]
        public async Task<IHttpActionResult> GetCategory(int id)
        {
            var category = await db.Categories.FindAsync(id);
            if (category == null)
            {
                return NotFound();
            }

            // return Ok(CategoryDto.ConvertFromModel(category));
            return Ok(DtoConverter.ConvertToCategoryDto(category));
        }

        // GET: api/Categories/5/details
        [Route("{id:int}/details")]
        [ResponseType(typeof(CategoryDetailDto))]
        public async Task<IHttpActionResult> GetCategoryDetail(int id)
        {
            var category = await db.Categories.FindAsync(id);
            if (category == null)
            {
                return NotFound();
            }

            return Ok(DtoConverter.ConvertToCategoryDetailDto(category));
        }

        // GET: api/Categories/5/products
        [Route("{id:int}/products")]
        public IQueryable<ProductDto> GetProductsInCategory(int id)
        {
            var products = db.Categories.Where(m => m.CategoryID == id).SelectMany(m => m.Products);

            return products.Select(DtoConverter.AsProductDto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CategoryExists(int id)
        {
            return db.Categories.Count(e => e.CategoryID == id) > 0;
        }
    }
}