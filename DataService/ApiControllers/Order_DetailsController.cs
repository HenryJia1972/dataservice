﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using DataService.Models;
using DataService.DTO;

namespace DataService.ApiControllers
{
    [RoutePrefix("api/Order_Details")]
    public class Order_DetailsController : ApiController
    {
        private NorthwindContext db = new NorthwindContext();

        // GET: api/Order_Details
        [Route("")]
        public IQueryable<Order_DetailDto> GetOrder_Details()
        {
            return db.Order_Details.Select(DtoConverter.AsOrderDetailDto);
        }

        // GET: api/Order_Details/5
        [Route("{id:int}")]
        [ResponseType(typeof(Order_DetailDto))]
        public async Task<IHttpActionResult> GetOrder_Detail(int id)
        {
            Order_Detail order_Detail = await db.Order_Details.FindAsync(id);
            if (order_Detail == null)
            {
                return NotFound();
            }

            return Ok(order_Detail);
        }

        // GET: odata/Order_Detail(10248,72)
        [EnableQuery]
        public SingleResult<Order_Detail> GetOrder_Detail([FromODataUri] int keyOrderID, [FromODataUri] int keyProductID)
        {
            return SingleResult.Create(db.Order_Details.Where(order_Detail => order_Detail.OrderID == keyOrderID && order_Detail.ProductID == keyProductID));
        }

        // GET: odata/Order_Detail(10248,72)/Order
        [EnableQuery]
        public SingleResult<Order> GetOrder([FromODataUri] int keyOrderID, [FromODataUri] int keyProductID)
        {
            return SingleResult.Create(db.Order_Details.Where(m => m.OrderID == keyOrderID && m.ProductID == keyProductID).Select(m => m.Order));
        }

        // GET: odata/Order_Detail(10248,72)/Product
        [HttpGet]
        [EnableQuery]
        public SingleResult<Product> GetProduct([FromODataUri] int keyOrderID, [FromODataUri] int keyProductID)
        {
            // return db.Order_Details.Where(m => m.OrderID == keyOrderID).Select(m => m.Product);
            return SingleResult.Create(db.Order_Details.Where(m => m.OrderID == keyOrderID && m.ProductID == keyProductID).Select(m => m.Product));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Order_DetailExists(int id)
        {
            return db.Order_Details.Count(e => e.OrderID == id) > 0;
        }
    }
}