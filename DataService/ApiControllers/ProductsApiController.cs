﻿using DataService.DTO;
using DataService.Models;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace DataService.ApiControllers
{
    [RoutePrefix("api/Products")]
    public class ProductsApiController : ApiController
    {
        private NorthwindContext db = new NorthwindContext();

        // GET: api/Products
        [Route("")]
        public IQueryable<ProductDto> GetProducts()
        {
            return db.Products.Select(DtoConverter.AsProductDto);
        }

        // GET: api/Products/5
        [Route("{id:int}")]
        [ResponseType(typeof(ProductDto))]
        public async Task<IHttpActionResult> GetProduct(int id)
        {
            Product product = await db.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            return Ok(DtoConverter.ConvertToProductDto(product));
        }

        // GET: api/Products/5/Category
        [Route("{id:int}/Category")]
        [ResponseType(typeof(CategoryDto))]
        public async Task<IHttpActionResult> GetCategory(int id)
        {
            Product product = await db.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            return Ok(DtoConverter.ConvertToCategoryDto(product.Category));
        }

        // GET: api/Products/5/Order_Details
        [Route("{id:int}/Order_Details")]
        public IQueryable<Order_DetailDto> GetOrder_Details(int id)
        {
            return db.Products.Where(m => m.ProductID == id).SelectMany(m => m.Order_Details).Select(DtoConverter.AsOrderDetailDto);
        }

        // GET: api/Products/5/Supplier
        [Route("{id:int}/Supplier")]
        [ResponseType(typeof(SupplierDto))]
        public async Task<IHttpActionResult> GetSupplier(int id)
        {
            Product product = await db.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            return Ok(DtoConverter.ConvertToSupplierDto(product.Supplier));
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductExists(int id)
        {
            return db.Products.Count(e => e.ProductID == id) > 0;
        }
    }
}