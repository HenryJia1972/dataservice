﻿using DataService.DTO;
using DataService.Models;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace DataService.ApiControllers
{
    [RoutePrefix("api/Suppliers")]
    public class SuppliersApiController : ApiController
    {
        private NorthwindContext db = new NorthwindContext();

        // GET: api/Suppliers
        [Route("")]
        public IQueryable<SupplierDto> GetSuppliers()
        {
            return db.Suppliers.Select(DtoConverter.AsSupplierDto);
        }

        // GET: api/Suppliers/5
        [Route("{id:int}")]
        [ResponseType(typeof(SupplierDto))]
        public async Task<IHttpActionResult> GetSupplier(int id)
        {
            Supplier supplier = await db.Suppliers.FindAsync(id);
            if (supplier == null)
            {
                return NotFound();
            }

            return Ok(DtoConverter.ConvertToSupplierDto(supplier));
        }

        // GET: api/Suppliers/5/Products
        [Route("{id:int}/Products")]
        public IQueryable<ProductDto> GetOrders(int id)
        {
            return db.Suppliers.Where(m => m.SupplierID == id).SelectMany(m => m.Products).Select(DtoConverter.AsProductDto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SupplierExists(int id)
        {
            return db.Suppliers.Count(e => e.SupplierID == id) > 0;
        }
    }
}