﻿using DataService.DTO;
using DataService.Models;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace DataService.ApiControllers
{
    [RoutePrefix("api/Shippers")]
    public class ShippersApiController : ApiController
    {
        private NorthwindContext db = new NorthwindContext();

        // GET: api/Shippers
        [Route("")]
        public IQueryable<ShipperDto> GetShippers()
        {
            return db.Shippers.Select(DtoConverter.AsShipperDto);
        }

        // GET: api/Shippers/5
        [Route("{id:int}")]
        [ResponseType(typeof(ShipperDto))]
        public async Task<IHttpActionResult> GetShipper(int id)
        {
            Shipper shipper = await db.Shippers.FindAsync(id);
            if (shipper == null)
            {
                return NotFound();
            }

            return Ok(DtoConverter.ConvertToShipperDto(shipper));
        }

        // GET: api/Shippers/5/Orders
        [Route("{id:int}/Orders")]
        public IQueryable<OrderDto> GetOrders(int id)
        {
            return db.Shippers.Where(m => m.ShipperID == id).SelectMany(m => m.Orders).Select(DtoConverter.AsOrderDto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ShipperExists(int id)
        {
            return db.Shippers.Count(e => e.ShipperID == id) > 0;
        }
    }
}