﻿using DataService.Models;
using Microsoft.AspNet.OData;
using System.Data;
using System.Linq;
using System.Web.Http;

namespace DataService.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using DataService.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Category_Sales_for_1997>("Category_Sales_for_1997");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class Category_Sales_for_1997Controller : ODataController
    {
        private NorthwindContext db = new NorthwindContext();

        // GET: odata/Category_Sales_for_1997
        [EnableQuery]
        public IQueryable<Category_Sales_for_1997> GetCategory_Sales_for_1997()
        {
            return db.Category_Sales_for_1997;
        }

        // GET: odata/Category_Sales_for_1997('Produce')
        [EnableQuery]
        public SingleResult<Category_Sales_for_1997> GetCategory_Sales_for_1997([FromODataUri] string key)
        {
            return SingleResult.Create(db.Category_Sales_for_1997.Where(category_Sales_for_1997 => category_Sales_for_1997.CategoryName == key));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Category_Sales_for_1997Exists(string key)
        {
            return db.Category_Sales_for_1997.Count(e => e.CategoryName == key) > 0;
        }
    }
}
