﻿using DataService.Models;
using Microsoft.AspNet.OData;
using System.Data;
using System.Linq;
using System.Web.Http;

namespace DataService.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using DataService.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Alphabetical_list_of_product>("Alphabetical_list_of_products");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class Alphabetical_list_of_productsController : ODataController
    {
        private NorthwindContext db = new NorthwindContext();

        // GET: odata/Alphabetical_list_of_products
        [EnableQuery]
        public IQueryable<Alphabetical_list_of_product> GetAlphabetical_list_of_products()
        {
            return db.Alphabetical_list_of_products;
        }

        // GET: odata/Alphabetical_list_of_products(5)
        [EnableQuery]
        public SingleResult<Alphabetical_list_of_product> GetAlphabetical_list_of_product([FromODataUri] int key)
        {
            return SingleResult.Create(db.Alphabetical_list_of_products.Where(alphabetical_list_of_product => alphabetical_list_of_product.ProductID == key));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Alphabetical_list_of_productExists(int key)
        {
            return db.Alphabetical_list_of_products.Count(e => e.ProductID == key) > 0;
        }
    }
}
