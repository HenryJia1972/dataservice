﻿using DataService.Models;
using Microsoft.AspNet.OData;
using System.Data;
using System.Linq;
using System.Web.Http;

namespace DataService.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using DataService.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Order>("Orders");
    builder.EntitySet<Customer>("Customers"); 
    builder.EntitySet<Employee>("Employees"); 
    builder.EntitySet<Order_Detail>("Order_Details"); 
    builder.EntitySet<Shipper>("Shippers"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class OrdersController : ODataController
    {
        private NorthwindContext db = new NorthwindContext();

        // GET: odata/Orders
        [EnableQuery]
        public IQueryable<Order> GetOrders()
        {
            return db.Orders;
        }

        // GET: odata/Orders(5)
        [EnableQuery]
        public SingleResult<Order> GetOrder([FromODataUri] int key)
        {
            return SingleResult.Create(db.Orders.Where(order => order.OrderID == key));
        }

        // GET: odata/Orders(5)/Customer
        [EnableQuery]
        public SingleResult<Customer> GetCustomer([FromODataUri] int key)
        {
            return SingleResult.Create(db.Orders.Where(m => m.OrderID == key).Select(m => m.Customer));
        }

        // GET: odata/Orders(5)/Employee
        [EnableQuery]
        public SingleResult<Employee> GetEmployee([FromODataUri] int key)
        {
            return SingleResult.Create(db.Orders.Where(m => m.OrderID == key).Select(m => m.Employee));
        }

        // GET: odata/Orders(5)/Order_Details
        [EnableQuery]
        public IQueryable<Order_Detail> GetOrder_Details([FromODataUri] int key)
        {
            return db.Orders.Where(m => m.OrderID == key).SelectMany(m => m.Order_Details);
        }

        // GET: odata/Orders(5)/Shipper
        [EnableQuery]
        public SingleResult<Shipper> GetShipper([FromODataUri] int key)
        {
            return SingleResult.Create(db.Orders.Where(m => m.OrderID == key).Select(m => m.Shipper));
        }

        //[EnableQuery]
        //[ODataRoute("Orders({key})/Products")]
        //public IQueryable<Product> GetProductsOfAnOrder([FromODataUri] int key)
        //{
        //    // return db.Order_Details.Where(m => m.OrderID == key).Select(m => m.Product);
        //    var query = db.Order_Details
        //        .Where(x => x.OrderID == key)
        //        .Join(db.Products, od => od.ProductID, p => p.ProductID, (od, p) => new { Product = p, OrderDetail = od })
        //        .Select(x => x.Product);

        //    return query;
        //    //var query = from o in db.Orders
        //    //            from p in db.Products
        //    //            join d in db.Order_Details
        //    //                on new { o.OrderID, p.ProductID } equals new { d.OrderID, d.ProductID } into details
        //    //            from d in details
        //    //            select p;

        //    //return query;
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrderExists(int key)
        {
            return db.Orders.Count(e => e.OrderID == key) > 0;
        }
    }
}
