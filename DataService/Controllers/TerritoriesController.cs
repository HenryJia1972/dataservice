﻿using DataService.Models;
using Microsoft.AspNet.OData;
using System.Data;
using System.Linq;
using System.Web.Http;

namespace DataService.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using DataService.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Territory>("Territories");
    builder.EntitySet<Employee>("Employees"); 
    builder.EntitySet<Region>("Regions"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class TerritoriesController : ODataController
    {
        private NorthwindContext db = new NorthwindContext();

        // GET: odata/Territories
        [EnableQuery]
        public IQueryable<Territory> GetTerritories()
        {
            return db.Territories;
        }

        // GET: odata/Territories(5)
        [EnableQuery]
        public SingleResult<Territory> GetTerritory([FromODataUri] string key)
        {
            return SingleResult.Create(db.Territories.Where(territory => territory.TerritoryID == key));
        }

        // GET: odata/Territories(5)/Employees
        [EnableQuery]
        public IQueryable<Employee> GetEmployees([FromODataUri] string key)
        {
            // return db.Territories.Where(m => m.TerritoryID == key).SelectMany(m => m.Employees);

            return db.EmployeeTerritories
                .Where(x => x.TerritoryID == key)
                .Join(db.Employees, et => et.EmployeeID, e => e.EmployeeID, (et, e) => new { Employee = e, EmployeeTerritory = et })
                .Select(x => x.Employee);
            // return db.EmployeeTerritories.Where(m => m.TerritoryID == key).SelectMany(m => m.Employees);
        }

        // GET: odata/Territories(5)/Region
        [EnableQuery]
        public SingleResult<Region> GetRegion([FromODataUri] string key)
        {
            return SingleResult.Create(db.Territories.Where(m => m.TerritoryID == key).Select(m => m.Region));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TerritoryExists(string key)
        {
            return db.Territories.Count(e => e.TerritoryID == key) > 0;
        }
    }
}
