﻿using DataService.Models;
using Microsoft.AspNet.OData;
using System.Data;
using System.Linq;
using System.Web.Http;

namespace DataService.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using DataService.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Region>("Regions");
    builder.EntitySet<Territory>("Territories"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class RegionsController : ODataController
    {
        private NorthwindContext db = new NorthwindContext();

        // GET: odata/Regions
        [EnableQuery]
        public IQueryable<Region> GetRegions()
        {
            return db.Regions;
        }

        // GET: odata/Regions(5)
        [EnableQuery]
        public SingleResult<Region> GetRegion([FromODataUri] int key)
        {
            return SingleResult.Create(db.Regions.Where(region => region.RegionID == key));
        }

        // GET: odata/Regions(5)/Territories
        [EnableQuery]
        public IQueryable<Territory> GetTerritories([FromODataUri] int key)
        {
            return db.Regions.Where(m => m.RegionID == key).SelectMany(m => m.Territories);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RegionExists(int key)
        {
            return db.Regions.Count(e => e.RegionID == key) > 0;
        }
    }
}
