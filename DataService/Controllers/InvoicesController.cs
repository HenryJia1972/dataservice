﻿using DataService.Models;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.OData;

namespace DataService.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using DataService.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Invoice>("Invoices");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class InvoicesController : ODataController
    {
        private NorthwindContext db = new NorthwindContext();

        // GET: odata/Invoices
        [EnableQuery]
        public IQueryable<Invoice> GetInvoices()
        {
            return db.Invoices;
        }

        // GET: odata/Invoices(10248,72)
        [EnableQuery]
        public SingleResult<Invoice> GetInvoice([FromODataUri] int keyOrderID, [FromODataUri] int keyProductID)
        {
            return SingleResult.Create(db.Invoices.Where(invoice => invoice.OrderID == keyOrderID && invoice.ProductID == keyProductID));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool InvoiceExists(string key)
        {
            return db.Invoices.Count(e => e.CustomerName == key) > 0;
        }
    }
}
