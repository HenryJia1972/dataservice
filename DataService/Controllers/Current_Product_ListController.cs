﻿using DataService.Models;
using Microsoft.AspNet.OData;
using System.Data;
using System.Linq;
using System.Web.Http;

namespace DataService.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using DataService.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Current_Product_List>("Current_Product_List");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class Current_Product_ListController : ODataController
    {
        private NorthwindContext db = new NorthwindContext();

        // GET: odata/Current_Product_List
        [EnableQuery]
        public IQueryable<Current_Product_List> GetCurrent_Product_List()
        {
            return db.Current_Product_Lists;
        }

        // GET: odata/Current_Product_List(5)
        [EnableQuery]
        public SingleResult<Current_Product_List> GetCurrent_Product_List([FromODataUri] int key)
        {
            return SingleResult.Create(db.Current_Product_Lists.Where(current_Product_List => current_Product_List.ProductID == key));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Current_Product_ListExists(int key)
        {
            return db.Current_Product_Lists.Count(e => e.ProductID == key) > 0;
        }
    }
}
