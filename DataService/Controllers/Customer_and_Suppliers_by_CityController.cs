﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.OData;
using System.Web.Http.OData.Routing;
using DataService.Models;

namespace DataService.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using DataService.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Customer_and_Suppliers_by_City>("Customer_and_Suppliers_by_City");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class Customer_and_Suppliers_by_CityController : ODataController
    {
        private NorthwindContext db = new NorthwindContext();

        // GET: odata/Customer_and_Suppliers_by_City
        [EnableQuery]
        public IQueryable<Customer_and_Suppliers_by_City> GetCustomer_and_Suppliers_by_City()
        {
            return db.Customer_and_Suppliers_by_Cities;
        }

        // GET: odata/Customer_and_Suppliers_by_City('Bend')
        [EnableQuery]
        public SingleResult<Customer_and_Suppliers_by_City> GetCustomer_and_Suppliers_by_City([FromODataUri] string key)
        {
            return SingleResult.Create(db.Customer_and_Suppliers_by_Cities.Where(customer_and_Suppliers_by_City => customer_and_Suppliers_by_City.City == key));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Customer_and_Suppliers_by_CityExists(string key)
        {
            return db.Customer_and_Suppliers_by_Cities.Count(e => e.CompanyName == key) > 0;
        }
    }
}
