﻿using DataService.Models;
using Microsoft.AspNet.OData;
using System.Data;
using System.Linq;
using System.Web.Http;

namespace DataService.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using DataService.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Shipper>("Shippers");
    builder.EntitySet<Order>("Orders"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ShippersController : ODataController
    {
        private NorthwindContext db = new NorthwindContext();

        // GET: odata/Shippers
        [EnableQuery]
        public IQueryable<Shipper> GetShippers()
        {
            return db.Shippers;
        }

        // GET: odata/Shippers(5)
        [EnableQuery]
        public SingleResult<Shipper> GetShipper([FromODataUri] int key)
        {
            return SingleResult.Create(db.Shippers.Where(shipper => shipper.ShipperID == key));
        }

        // GET: odata/Shippers(5)/Orders
        [EnableQuery]
        public IQueryable<Order> GetOrders([FromODataUri] int key)
        {
            return db.Shippers.Where(m => m.ShipperID == key).SelectMany(m => m.Orders);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ShipperExists(int key)
        {
            return db.Shippers.Count(e => e.ShipperID == key) > 0;
        }
    }
}
