﻿using DataService.Models;
using Microsoft.AspNet.OData;
using System.Data;
using System.Linq;
using System.Web.Http;

namespace DataService.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using DataService.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Order_Detail>("Order_Detail");
    builder.EntitySet<Order>("Orders"); 
    builder.EntitySet<Product>("Products"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class Order_DetailsController : ODataController
    {
        private NorthwindContext db = new NorthwindContext();

        // GET: odata/Order_Detail
        [EnableQuery]
        public IQueryable<Order_Detail> GetOrder_Details()
        {
            return db.Order_Details;
        }

        // GET: odata/Order_Detail(10248,72)
        [EnableQuery]
        public SingleResult<Order_Detail> GetOrder_Detail([FromODataUri] int keyOrderID, [FromODataUri] int keyProductID)
        {
            return SingleResult.Create(db.Order_Details.Where(order_Detail => order_Detail.OrderID == keyOrderID && order_Detail.ProductID == keyProductID));
        }

        // GET: odata/Order_Detail(10248,72)/Order
        [EnableQuery]
        public SingleResult<Order> GetOrder([FromODataUri] int keyOrderID, [FromODataUri] int keyProductID)
        {
            return SingleResult.Create(db.Order_Details.Where(m => m.OrderID == keyOrderID && m.ProductID == keyProductID).Select(m => m.Order));
        }

        // GET: odata/Order_Detail(10248,72)/Product
        [HttpGet]
        [EnableQuery]
        public SingleResult<Product> GetProduct([FromODataUri] int keyOrderID, [FromODataUri] int keyProductID)
        {
            // return db.Order_Details.Where(m => m.OrderID == keyOrderID).Select(m => m.Product);
            return SingleResult.Create(db.Order_Details.Where(m => m.OrderID == keyOrderID && m.ProductID == keyProductID).Select(m => m.Product));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Order_DetailExists(int key)
        {
            return db.Order_Details.Count(e => e.OrderID == key) > 0;
        }
    }
}
